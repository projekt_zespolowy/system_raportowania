﻿using System;
using System.Net.Mail;



namespace WysylaniePoczty
{
    class Program
    {
        static void Main()
        {
            //KONFIGURACJA WIADOMOŚCI:
            MailMessage message = new MailMessage();
            message.Sender = new MailAddress("241487@student.pwr.edu.pl");
            message.From = new MailAddress("241487@student.pwr.edu.pl");
            message.To.Add (new MailAddress("248572@student.pwr.edu.pl"));
            message.CC.Add (new MailAddress("248572@student.pwr.edu.pl"));
            message.Subject = "Próba";
            message.Body = "Przesyłam ci zdjęcie w załączniku";
            message.IsBodyHtml = false;
            message.Priority = MailPriority.Normal;
            Attachment firstAttachment = new Attachment("zdiecie.jpg", System.Net.Mime.MediaTypeNames.Image.Jpeg);
            message.Attachments.Add(firstAttachment);

            //KONFIGURACJA POŁĄCZENIA:
            //Port SMTP używany przez student.pwr.edu.pl to: 587 lub 465
            SmtpClient client = new SmtpClient("student.pwr.edu.pl", 587); //Klient (Simple Mail Transfer Protocol)
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential("241487@student.pwr.edu.pl", "****"); // Pamiętaj o ustawieniu prawidłowych danych dostępowych

            //WYSŁANIE WIADOMOSCI:
            client.Send(message);

        }
    }
}
