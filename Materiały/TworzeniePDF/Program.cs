﻿using PdfSharp.Drawing;
using PdfSharp.Drawing.Layout;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TworzeniePDF
{
    class Program
    {

        static void Main(string[] args)
        {

            var pdfNewDocument = new PdfDocument();
            pdfNewDocument.AddPage();
            var gfx = XGraphics.FromPdfPage(pdfNewDocument.Pages[0]);

            var font = new XFont("Arial", 12, XFontStyle.Regular);
            var bigFont = new XFont("Arial", 14, XFontStyle.Bold);
            var veryBigFont = new XFont("Arial", 20, XFontStyle.Bold);

            //  gfx.DrawString("Hello World", font, XBrushes.Black, new XPoint(300, 840)); //595 --- 840 |   Pisanie bezpośrednio po pikselach


            gfx.DrawString("Przykładowy raport z produkcji", veryBigFont, XBrushes.DarkBlue, new XRect(0, 40, pdfNewDocument.Pages[0].Width, pdfNewDocument.Pages[0].Height), XStringFormats.TopCenter);
            gfx.DrawString("Dane diagnosyczne", bigFont, XBrushes.Black, new XRect(40, 80, pdfNewDocument.Pages[0].Width - 80, 20), XStringFormats.CenterLeft);
            gfx.DrawLine(new XPen(XColor.FromKnownColor(XKnownColor.Black)), new XPoint(40, 100), new XPoint(pdfNewDocument.Pages[0].Width - 40, 100));


            const string text1 =
            "Zmiennna numer 1:\n" +
            "Zmiennna numer 2:\n" +
            "Zmiennna numer 3:\n" +
            "Zmiennna numer 4:\n" +
            "Zmiennna numer 5:\n";
            const string text2 =
            "Zmiennna numer 6:\n" +
            "Zmiennna numer 7:\n" +
            "Zmiennna numer 8:\n" +
            "Zmiennna numer 9:\n" +
            "Zmiennna numer 10:\n";



            XRect rect1 = new XRect(40, 110, (pdfNewDocument.Pages[0].Width -80)/2 , 80);
            XTextFormatter tf1 = new XTextFormatter(gfx);
            tf1.Alignment = XParagraphAlignment.Justify;
            tf1.DrawString(text1, font, XBrushes.Black, rect1, XStringFormats.TopLeft);

            XRect rect2 = new XRect((pdfNewDocument.Pages[0].Width)/2, 110, (pdfNewDocument.Pages[0].Width) / 2 - 80, 80);
            XTextFormatter tf2 = new XTextFormatter(gfx);
            tf2.Alignment = XParagraphAlignment.Justify;
            tf2.DrawString(text2, font, XBrushes.Black, rect2, XStringFormats.TopLeft);

            gfx.DrawString("Dane produkcyjne", bigFont, XBrushes.Black, new XRect(40, 190, pdfNewDocument.Pages[0].Width - 80, 20), XStringFormats.CenterLeft);
            gfx.DrawLine(new XPen(XColor.FromKnownColor(XKnownColor.Black)), new XPoint(40, 210), new XPoint(pdfNewDocument.Pages[0].Width - 40, 210));


            XRect rect3 = new XRect(40, 220, (pdfNewDocument.Pages[0].Width - 80) / 2, 80);
            XTextFormatter tf3 = new XTextFormatter(gfx);
            tf3.Alignment = XParagraphAlignment.Justify;
            tf3.DrawString(text1, font, XBrushes.Black, rect3, XStringFormats.TopLeft);

            XRect rect4 = new XRect((pdfNewDocument.Pages[0].Width) / 2, 220, (pdfNewDocument.Pages[0].Width) / 2 - 80, 80);
            XTextFormatter tf4 = new XTextFormatter(gfx);
            tf4.Alignment = XParagraphAlignment.Justify;
            tf4.DrawString(text2, font, XBrushes.Black, rect4, XStringFormats.TopLeft);


            // gfx.DrawLine(new XPen(XColor.FromKnownColor(XKnownColor.Black)),new XPoint(40,250), new XPoint(pdfNewDocument.Pages[0].Width - 40,250));


            pdfNewDocument.Save("nowyDokument.pdf");
            Process.Start("nowyDokument.pdf");

        }
    }
}
